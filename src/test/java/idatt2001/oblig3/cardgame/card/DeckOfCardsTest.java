package idatt2001.oblig3.cardgame.card;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {

    @Test
    void correctNumberOfCardsinDealHand() {
        DeckOfCards deckOfCards = new DeckOfCards();
        assertEquals(5,deckOfCards.dealHand(5).size());
    }
}