package idatt2001.oblig3.cardgame;

import idatt2001.oblig3.cardgame.card.DeckOfCards;
import idatt2001.oblig3.cardgame.card.HandOfCards;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.Parent;

import java.lang.invoke.VarHandle;

public class App extends Application {


    @Override
    public void start(Stage stage) throws Exception {
        Group group = new Group();
        stage.setTitle("Card Game Application");
        HandOfCards handOfCards = new HandOfCards();
        Label sumOfFaces = new Label();
        sumOfFaces.setLayoutY(100);
        sumOfFaces.setText("Sum of Faces: ");
        Label cardsOfHearts = new Label();
        cardsOfHearts.setText("Cards Of Hearts:");
        cardsOfHearts.setLayoutY(150);
        Label isFlush = new Label();
        isFlush.setText("Flush:");
        isFlush.setLayoutY(200);
        Label isQueenOfSpades= new Label();
        isQueenOfSpades.setText("Queen of Spades:");
        isQueenOfSpades.setLayoutY(250);
        Label cardHand = new Label();
        cardHand.setText("Hand of Cards");
        cardHand.setLayoutY(300);


        Button checkHand = new Button();
        checkHand.setLayoutY(350);
        checkHand.setText("Check Hand");
        checkHand.setOnAction(actionEvent ->{
            isFlush.setText("Flush:" + handOfCards.flush());
            isQueenOfSpades.setText("Queen of Spades: " + handOfCards.queenOfSpades());
            cardsOfHearts.setText("Cards Of Hearts: " + handOfCards.cardsOfHearts());
            sumOfFaces.setText("Sum of Faces: " + handOfCards.sumOfFaces());
        } );


        Button dealHand = new Button();
        dealHand.setText("Deal Hand");
        dealHand.setLayoutY(350);
        dealHand.setLayoutX(80);
        dealHand.setOnAction(actionEvent ->{
            handOfCards.dealHand(5);
            sumOfFaces.setText("Sum of Faces");
            cardsOfHearts.setText("Cards Of Hearts");
            isQueenOfSpades.setText("Queen of Spades:");
            isFlush.setText("Flush:");
            cardHand.setText(handOfCards.displayHand());

        });
        group.getChildren().add(sumOfFaces);
        group.getChildren().add(cardsOfHearts);
        group.getChildren().add(cardHand);
        group.getChildren().add(checkHand);
        group.getChildren().add(dealHand);
        group.getChildren().add(isFlush);
        group.getChildren().add(isQueenOfSpades);

        stage.setScene(new Scene(group,1600,900));
        stage.show();



    }
}
