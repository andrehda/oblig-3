package idatt2001.oblig3.cardgame.card;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

public class HandOfCards{
    private ArrayList<PlayingCard> playingCards;
    DeckOfCards deckOfCards = new DeckOfCards();

    public HandOfCards() {
        playingCards = new ArrayList<>();
    }
    public void dealHand(int i){
        playingCards = deckOfCards.dealHand(i);
    }

    public boolean flush(){
        if(playingCards.stream().filter(card->card.getSuit()=='H').count()>4){
            return true;
        }
        else if(playingCards.stream().filter(card->card.getSuit()=='S').count()>4){
            return true;
        }
        else if(playingCards.stream().filter(card->card.getSuit()=='C').count()>4){
            return true;
        }
        else if(playingCards.stream().filter(card->card.getSuit()=='D').count()>4){
            return true;
        }else {
            return false;
        }
    }
    public boolean queenOfSpades(){
        return playingCards.stream().anyMatch(card -> card.getSuit()=='S'&& card.getFace()==12);
    }
    public int sumOfFaces(){
        return playingCards.stream().mapToInt(PlayingCard::getFace).sum();
    }
    public String cardsOfHearts() {
        return playingCards.stream().filter(card -> card.getSuit() == 'H').map(PlayingCard::toString).collect(Collectors.joining(" "));
    }
    public String displayHand(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(playingCards);
        return stringBuilder.toString();
    }
}