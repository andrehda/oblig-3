package idatt2001.oblig3.cardgame.card;

import java.util.ArrayList;
import java.util.Random;

public class DeckOfCards {
    ArrayList<PlayingCard> cardDeck = new ArrayList<>();
    Random random = new Random();
    private final char[] suit = { 'S', 'H', 'D', 'C' };

    public DeckOfCards() {
        for(int i = 1; i<13; i++){
            for(int j = 0; j<4; j++){
                cardDeck.add(new PlayingCard(suit[j],i));
            }
        }
    }
    public ArrayList<PlayingCard> dealHand(int n){
        ArrayList<PlayingCard> hand = new ArrayList<>();
        for(int i = 0; i < n; i++){
            hand.add(cardDeck.get(random.nextInt(cardDeck.size())));
        }
        return hand;
    }
}
