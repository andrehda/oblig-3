module idatt2001.oblig3.cardgame {
    requires javafx.controls;
    requires javafx.fxml;

    opens idatt2001.oblig3.cardgame to javafx.fxml;
    exports idatt2001.oblig3.cardgame;
        }